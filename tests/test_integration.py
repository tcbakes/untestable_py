import unittest
import json
from mock import create_autospec
from mock import call
from collections import OrderedDict

from untestable.i_o.console_io import UserIO
from untestable.bitbucket_app import BitbucketApp
from untestable.i_o.credential_persistence import CredentialPersistence
from untestable.i_o.credentials import MissingCredentials
from untestable.flow.simple_credentials_collection_flow import SimpleCredentialsCollectionFlow
from untestable.i_o.bitbucket_client import BitbucketClient
from untestable.i_o.bitbucket_client import BitbucketResponse
from untestable.commands.g_user_command import GUserCommand
from untestable.commands.g_user_repos_command import GUserReposCommand


class IntegrationTest(unittest.TestCase):

    def setUp(self):
        self.mock_io = create_autospec(UserIO)
        self.mock_creds = create_autospec(CredentialPersistence)
        self.mock_bbclient = create_autospec(BitbucketClient)
        _flow = SimpleCredentialsCollectionFlow(self.mock_io, self.mock_creds)
        _guser = GUserCommand(self.mock_io, _flow, self.mock_bbclient)
        _guserrepos = GUserReposCommand(self.mock_io, _flow, self.mock_bbclient)
        self._app = BitbucketApp(self.mock_io, _guser, _guserrepos)

    def test1(self):
        """
        When g-user command is run and there are no saved credentials, the user is prompted for the
        credentials, at which point they enter valid credentials, the user is then asked if they
        would like their credentials remembered, to which the user responds "no".  The application
        then passes the valid credentials along to the Bitbucket GetUser service, and the response
        is formatted and written nicely to the output
        """

        self.mock_creds.retrieve.side_effect = [MissingCredentials()]
        self.mock_io.collect.side_effect = ["tcbakes", "n"]
        self.mock_io.collect_sensitive.side_effect = ["something"]
        self.mock_bbclient.get_user.side_effect = [BitbucketResponse(200, "OK", json.loads(
            """
            {"repositories": [],
            "user": {
            "is_staff": false,
            "resource_uri": "/1.0/users/tcbakes",
            "last_name": "Baker",
            "avatar": "https://bitbucket.org/account/tcbakes/avatar/32/?ts=0",
            "display_name": "Tristan Baker",
            "first_name": "Tristan",
            "is_team": false,
            "username": "tcbakes"}}
            """
            , object_pairs_hook=OrderedDict))]

        self._app.run(["g-user"])

        self.mock_io.collect.assert_has_calls([call("Username: "), call("Remember [y/n]: ")])
        self.mock_io.collect_sensitive.assert_called_with("Password: ")
        self.mock_creds.persist.assert_not_called()
        self.mock_io.display.assert_called_once_with(
"""{
    "user":{
        "avatar":"https://bitbucket.org/account/tcbakes/avatar/32/?ts=0",
        "display_name":"Tristan Baker",
        "first_name":"Tristan",
        "is_staff":false,
        "is_team":false,
        "last_name":"Baker",
        "resource_uri":"/1.0/users/tcbakes",
        "username":"tcbakes"
    }
}"""
        )

    def test2(self):
        """
        When g-user-repos command is run and there are no saved credentials, the user is prompted
        for the credentials, at which point they enter valid credentials, the user is then asked if
        they would like their credentials remembered, to which the user responds "no".  The
        application then passes the valid credentials along to the Bitbucket GetUserRepos service,
        and the response is formatted and written nicely to the output
        """

        self.mock_creds.retrieve.side_effect = [MissingCredentials()]
        self.mock_io.collect.side_effect = ["tcbakes", "n"]
        self.mock_io.collect_sensitive.side_effect = ["something"]
        payload = (
            """[{
            "is_private": false,
            "no_public_forks": false,
            "description": "Illustrates the evolution of an application from untestable to testable.",
            "language": "java",
            "has_wiki": false,
            "read_only": false,
            "logo": "https://bitbucket.org/tcbakes/untestable/avatar/32/?ts=1445056854",
            "state": "available",
            "scm": "git",
            "slug": "untestable",
            "owner": "tcbakes",
            "utc_created_on": "2015-10-15 04:45:18+00:00",
            "is_fork": false,
            "last_updated": "2015-10-17T06:40:54.696",
            "website": "",
            "creator": null,
            "is_mq": false,
            "mq_of": null,
            "utc_last_updated": "2015-10-17 04:40:54+00:00",
            "resource_uri": "/1.0/repositories/tcbakes/untestable",
            "fork_of": null,
            "has_issues": false,
            "no_forks": false,
            "email_mailinglist": "",
            "size": 188561,
            "created_on": "2015-10-15T06:45:18.942",
            "name": "untestable"
            }]"""
        )

        self.mock_bbclient.get_user_repositories.side_effect = [
            BitbucketResponse(200,
                              "OK",
                              json.loads(payload,
                                         object_pairs_hook=OrderedDict))
        ]
        self._app.run(["g-user-repos"])

        self.mock_io.collect.assert_has_calls([call("Username: "), call("Remember [y/n]: ")])
        self.mock_io.collect_sensitive.assert_called_with("Password: ")
        self.mock_creds.persist.assert_not_called()
        self.mock_io.display.assert_called_once_with(
"""[
    {
        "is_private":false,
        "no_public_forks":false,
        "description":"Illustrates the evolution of an application from untestable to testable.",
        "language":"java",
        "has_wiki":false,
        "read_only":false,
        "logo":"https://bitbucket.org/tcbakes/untestable/avatar/32/?ts=1445056854",
        "state":"available",
        "scm":"git",
        "slug":"untestable",
        "owner":"tcbakes",
        "utc_created_on":"2015-10-15 04:45:18+00:00",
        "is_fork":false,
        "last_updated":"2015-10-17T06:40:54.696",
        "website":"",
        "creator":null,
        "is_mq":false,
        "mq_of":null,
        "utc_last_updated":"2015-10-17 04:40:54+00:00",
        "resource_uri":"/1.0/repositories/tcbakes/untestable",
        "fork_of":null,
        "has_issues":false,
        "no_forks":false,
        "email_mailinglist":"",
        "size":188561,
        "created_on":"2015-10-15T06:45:18.942",
        "name":"untestable"
    }
]"""
        )