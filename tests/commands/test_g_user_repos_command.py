from collections import OrderedDict
import unittest
import json

from mock import create_autospec
from untestable.i_o.user_io import UserIO
from untestable.i_o.bitbucket_client import BitbucketClient
from untestable.i_o.bitbucket_client import BitbucketResponse
from untestable.flow.flows import CredentialsFlow
from untestable.i_o.credentials import Credentials
from untestable.commands.g_user_repos_command import GUserReposCommand


class ArgDrivenAppTest(unittest.TestCase):

    def setUp(self):

        self.mock_io = create_autospec(UserIO)
        self.mock_flow = create_autospec(CredentialsFlow)
        self.mock_bb_client = create_autospec(BitbucketClient)
        self.command = GUserReposCommand(self.mock_io,
                                         self.mock_flow,
                                         self.mock_bb_client)

    def test1(self):
        """
        The command should appropriately handle errors from the service and display a message
        """

        # When there are credentials
        self.mock_flow.run.side_effect = [Credentials("tcbakes", "fake-o")]

        # And when the bitbucket getUser service responds with an error
        self.mock_bb_client.get_user_repositories.side_effect = [BitbucketResponse(401,
                                                                                   "Unauthorized",
                                                                                   None)]

        # And the command is run
        self.command.run()

        # The command should display the a simple explanation of the cause of the error
        self.mock_io.display.assert_called_once_with("Problem calling the service. "
                                                     "Response code: 401")

    def test2(self):
        """
        The command should appropriately display the user profile when it is
        successfully returned from the service.  It should remove the
        "repositories" property from the payload prior to displaying it.
        """

        # When there are credentials
        self.mock_flow.run.side_effect = [Credentials("tcbakes", "fake-o")]

        # And when the bitbucket getUser service responds with user profile
        # information, not nicely formatted (note that in this payload,
        # indentation has been removed)
        payload = (
            """[{
            "is_private": false,
            "no_public_forks": false,
            "description": "Illustrates the evolution of an application from untestable to testable.",
            "language": "java",
            "has_wiki": false,
            "read_only": false,
            "logo": "https://bitbucket.org/tcbakes/untestable/avatar/32/?ts=1445056854",
            "state": "available",
            "scm": "git",
            "slug": "untestable",
            "owner": "tcbakes",
            "utc_created_on": "2015-10-15 04:45:18+00:00",
            "is_fork": false,
            "last_updated": "2015-10-17T06:40:54.696",
            "website": "",
            "creator": null,
            "is_mq": false,
            "mq_of": null,
            "utc_last_updated": "2015-10-17 04:40:54+00:00",
            "resource_uri": "/1.0/repositories/tcbakes/untestable",
            "fork_of": null,
            "has_issues": false,
            "no_forks": false,
            "email_mailinglist": "",
            "size": 188561,
            "created_on": "2015-10-15T06:45:18.942",
            "name": "untestable"
            }]"""
        )

        self.mock_bb_client.get_user_repositories.side_effect = [
            BitbucketResponse(200,
                              "OK",
                              json.loads(payload,
                                         object_pairs_hook=OrderedDict))
        ]

        # And the command is run
        self.command.run()

        # The command should display the user profile information,
        # formatted nicely, with the "repositories" property removed
        self.mock_io.display.assert_called_once_with(self.test2.response)

    test2.response = (
"""[
    {
        "is_private":false,
        "no_public_forks":false,
        "description":"Illustrates the evolution of an application from untestable to testable.",
        "language":"java",
        "has_wiki":false,
        "read_only":false,
        "logo":"https://bitbucket.org/tcbakes/untestable/avatar/32/?ts=1445056854",
        "state":"available",
        "scm":"git",
        "slug":"untestable",
        "owner":"tcbakes",
        "utc_created_on":"2015-10-15 04:45:18+00:00",
        "is_fork":false,
        "last_updated":"2015-10-17T06:40:54.696",
        "website":"",
        "creator":null,
        "is_mq":false,
        "mq_of":null,
        "utc_last_updated":"2015-10-17 04:40:54+00:00",
        "resource_uri":"/1.0/repositories/tcbakes/untestable",
        "fork_of":null,
        "has_issues":false,
        "no_forks":false,
        "email_mailinglist":"",
        "size":188561,
        "created_on":"2015-10-15T06:45:18.942",
        "name":"untestable"
    }
]"""
    )

    def test3(self):
        """
        The command is named 'g-user-repos'
        """
        self.assertEqual("g-user-repos", self.command.name())
