import unittest
import json
from mock import create_autospec
import requests
from untestable.i_o.rest_bitbucket_client import RestBitBucketClient


class build_bb_response_test(unittest.TestCase):

    def setUp(self):
        self._mockResp = create_autospec(requests.Response)
        return

    def tearDown(self):
        return

    def test1(self):
        """
        Test that a 200 response with a JSON body results in a BitbucketResponse with that JSON
        body parsed into a json object
        """

        self._mockResp.json.side_effect = [json.loads('{"stuff": 12345}')]
        self._mockResp.reason = "OK"
        self._mockResp.status_code = 200

        bb_response = RestBitBucketClient.build_bb_response(self._mockResp)

        self.assertEqual(bb_response.status_code, 200)
        self.assertEqual(bb_response.reason, "OK")
        self.assertEqual(json.dumps(bb_response.body), '{"stuff": 12345}')

    def test2(self):
        """
        Test that a non-200 response with a text body results in a BitbucketResponse with that text
        """
        self._mockResp.text = "stuff"
        self._mockResp.reason = "Not Found"
        self._mockResp.status_code = 404

        bb_response = RestBitBucketClient.build_bb_response(self._mockResp)

        self.assertEqual(bb_response.status_code, 404)
        self.assertEqual(bb_response.reason, "Not Found")
        self.assertEqual(bb_response.body, "stuff")
