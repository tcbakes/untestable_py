import unittest
from mock import create_autospec

from untestable.i_o.user_io import UserIO
from untestable.commands.command import Command
from untestable.bitbucket_app import BitbucketApp


class ArgDrivenAppTest(unittest.TestCase):

    def setUp(self):

        self.mock_io = create_autospec(UserIO)
        self.mock_command = create_autospec(Command)
        self.bitbucket_app = BitbucketApp(self.mock_io, self.mock_command)

    def test1(self):
        """
        A handle-able command should be delegated to that command.
        """

        # When a command can handle the user's command request
        self.mock_command.can_handle.side_effect = [True]

        # And the app is run
        self.bitbucket_app.run(["some_command_name"])

        # The app should delegate the running of the command to the command instance
        self.mock_command.can_handle.assert_called_once_with("some_command_name")
        self.mock_command.run.assert_called_once_with()

    def test2(self):
        """
        An unrecognized command should display an error to the user.
        """

        # When no command exists capable of handling the user's command request
        self.mock_command.can_handle.side_effect = [False]

        # And the app is run
        self.bitbucket_app.run(["some_command_name"])

        # The app should not call the command
        self.mock_command.run.assert_not_called()

        # The app should display a message back to the user
        # explaining that the provided command is not supported
        self.mock_io.display.assert_called_once_with("Unrecognized command 'some_command_name'")

    def test3(self):
        """
        Print a message listing supported commands if no command is provided
        """
        # When there is at least one command
        self.mock_command.name.side_effect = ["some_command_name"]

        # And the app is run with no command
        self.bitbucket_app.run([])

        # The app should display a message listing the supported commands
        self.mock_io.display.assert_called_once_with(self.test3.response)

    test3.response = (
"""No command provided. Nothing to do.
Supported commands are:
some_command_name
"""
    )
