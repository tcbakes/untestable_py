Untestable Python System
=======================

A python equivalent of the java application here: https://bitbucket.org/tcbakes/untestable.  The purpose of this project
is to demonstrate how the patterns used in the original Java application can be applied to a python application.
The original Java Untestable project will always be the home for the detailed evolutionary history of the system from its
first spaghetti implementation to final elegant, testable one.  So the deliberate evolutionary steps will not be
repeated here.  For that level of insight, refer to the original project.

## Vanilla Installation

    $>pip install untestable
    $>untestable
    No command provided. Nothing to do.
    Supported commands are:
    g-user
    g-user-repos

## Packaging for Installation to a Remote Machine with no access to PyPI

    $>mkdir temp-wheelhouse
    $>pip wheel --wheel-dir=temp-wheelhouse untestable
    $>tar -zcvf untestable-full-install.tar.gz temp-wheelhouse/
    $>scp untestable-full-install.tar.gz user@remote.machine.com:~
    $>ssh user@remote.machine.com
    $[user@remote.machine.com]>tar -zxvf untestable-full-install.tar.gz 
    $[user@remote.machine.com]>pip install --no-index --find-links=temp-wheelhouse untestable
    $[user@remote.machine.com]>untestable
    No command provided. Nothing to do.
    Supported commands are:
    g-user
    g-user-repos

## Dev Environment Setup

    $>git clone https://tcbakes@bitbucket.org/tcbakes/untestable.git
    $>cd untestable
    $>mkvirtualenv untestable-dev
    (untestable-dev)$>pip install -e .[dev]
    (untestable-dev)$>ipython
    In [1]: import untestable
    In [2]: exit()
    (untestable-dev)$>pylint untestable
    ...[pylint report]...
    (untestable-dev)$>nosetests tests --with-coverage --cover-erase --cover-package=untestable
    ...[coverage report]...

## Release Environment Setup

    $>git clone https://tcbakes@bitbucket.org/tcbakes/untestable.git
    $>cd untestable-rel
    $>mkvirtualenv untestable-rel
    (untestable-rel)$>pip install -e .[rel]
    (untestable-rel)$>nosetests tests --with-coverage --cover-erase --cover-package=untestable --cover-min-percentage=[some value]
    (untestable-rel)$>bumpversion release
    (untestable-rel)$>pip wheel .
    (untestable-rel)$>python setup.py bdist_wheel upload
    (untestable-rel)$>bumpversion minor