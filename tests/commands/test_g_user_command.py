from collections import OrderedDict
import unittest
import json

from mock import create_autospec
from untestable.i_o.user_io import UserIO
from untestable.i_o.bitbucket_client import BitbucketClient
from untestable.i_o.bitbucket_client import BitbucketResponse
from untestable.flow.flows import CredentialsFlow
from untestable.i_o.credentials import Credentials
from untestable.commands.g_user_command import GUserCommand


class ArgDrivenAppTest(unittest.TestCase):

    def setUp(self):

        self.mock_io = create_autospec(UserIO)
        self.mock_flow = create_autospec(CredentialsFlow)
        self.mock_bb_client = create_autospec(BitbucketClient)
        self.command = GUserCommand(self.mock_io,
                                    self.mock_flow,
                                    self.mock_bb_client)

    def test1(self):
        """
        The command should appropriately handle errors from the service and display a message
        """

        # When there are credentials
        self.mock_flow.run.side_effect = [Credentials("tcbakes", "fake-o")]

        # And when the bitbucket getUser service responds with an error
        self.mock_bb_client.get_user.side_effect = [BitbucketResponse(401, "Unauthorized", None)]

        # And the command is run
        self.command.run()

        # The command should display a simple explanation of the error cause
        self.mock_io.display.assert_called_once_with("Problem calling the service."
                                                     " Response code: 401")

    def test2(self):
        """
        The command should appropriately display the user profile when it is
        successfully returned from the service.  It should remove the
        "repositories" property from the payload prior to displaying it.
        """

        # When there are credentials
        self.mock_flow.run.side_effect = [Credentials("tcbakes", "fake-o")]

        # And when the bitbucket getUser service responds with user profile
        # information, not nicely formatted (note that in this payload,
        # indentation has been removed)
        self.mock_bb_client.get_user.side_effect = [BitbucketResponse(200, "OK", json.loads(
            """
            {"repositories": [],
            "user": {
            "is_staff": false,
            "resource_uri": "/1.0/users/tcbakes",
            "last_name": "Baker",
            "avatar": "https://bitbucket.org/account/tcbakes/avatar/32/?ts=0",
            "display_name": "Tristan Baker",
            "first_name": "Tristan",
            "is_team": false,
            "username": "tcbakes"}}
            """
            , object_pairs_hook=OrderedDict))]

        # And the command is run
        self.command.run()

        # The command should display the user profile information,
        # formatted nicely, with the "repositories" property removed
        self.mock_io.display.assert_called_once_with(self.test2.response)

    test2.response = (
"""{
    "user":{
        "avatar":"https://bitbucket.org/account/tcbakes/avatar/32/?ts=0",
        "display_name":"Tristan Baker",
        "first_name":"Tristan",
        "is_staff":false,
        "is_team":false,
        "last_name":"Baker",
        "resource_uri":"/1.0/users/tcbakes",
        "username":"tcbakes"
    }
}"""
    )

    def test3(self):
        """
        The command is named 'g-user'
        """
        self.assertEqual("g-user", self.command.name())
