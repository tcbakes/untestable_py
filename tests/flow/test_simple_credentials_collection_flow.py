import unittest

from mock import call
from mock import create_autospec
from untestable.i_o.user_io import UserIO
from untestable.i_o.credentials import Credentials, MissingCredentials
from untestable.i_o.credential_persistence import CredentialPersistence
from untestable.flow.simple_credentials_collection_flow import SimpleCredentialsCollectionFlow


class ArgDrivenAppTest(unittest.TestCase):

    def setUp(self):
        self.mock_io = create_autospec(UserIO)
        self.mock_creds_persist = create_autospec(CredentialPersistence)
        self.flow = SimpleCredentialsCollectionFlow(self.mock_io, self.mock_creds_persist)

    def test1(self):
        """
        When there are no stored credentials, the flow should collect them from the user
        """

        # When there are no stored credentials
        self.mock_creds_persist.retrieve.side_effect = [MissingCredentials()]
        # And when a username is provided
        # And when the user doesn't want to have the credentials remembered
        self.mock_io.collect.side_effect = ["tcbakes", "n"]
        # And when a password is provided
        self.mock_io.collect_sensitive.side_effect = ["fake-o"]

        # And the flow is run
        creds = self.flow.run()

        # The flow should collect the username
        # The command should ask if the user wants their credentials remembered
        self.mock_io.collect.assert_has_calls([call("Username: "), call("Remember [y/n]: ")])
        # The flow should collect the password as a piece of sensitive information
        self.mock_io.collect_sensitive.assert_called_with("Password: ")
        # The command should not persist the credentials
        self.mock_creds_persist.persist.assert_not_called()
        # And the flow should return the collected credentials
        self.assertEqual(creds.username, "tcbakes")
        self.assertEqual(creds.password, "fake-o")

    def test2(self):
        """
        When there are stored credentials, the flow should not collect them
        from the user but simply collect them from the persisted location
        """

        # When there are saved and valid credentials
        self.mock_creds_persist.retrieve.side_effect = [Credentials("tcbakes", "fake-o")]

        # And the flow is run
        creds = self.flow.run()

        # The command should not collect the username
        self.mock_io.collect.assert_not_called()
        # The command should not collect the password
        self.mock_io.collect_sensitive.assert_not_called()
        # The command should not ask if the user wants their credentials remembered

        # And the flow should return the persisted credentials
        self.assertEqual(creds.username, "tcbakes")
        self.assertEqual(creds.password, "fake-o")

    def test3(self):
        """
        When the user elects to save the credentials, they should be saved
        """

        # When there are no stored credentials
        self.mock_creds_persist.retrieve.side_effect = [MissingCredentials()]
        # And when a username is provided
        # And when the user doesn't want to have the credentials remembered
        self.mock_io.collect.side_effect = ["tcbakes", "y"]
        # And when a password is provided
        self.mock_io.collect_sensitive.side_effect = ["fake-o"]

        # And the flow is run
        creds = self.flow.run()

        # The flow should collect the username
        # The command should ask if the user wants their credentials remembered
        self.mock_io.collect.assert_has_calls([call("Username: "), call("Remember [y/n]: ")])
        # The flow should collect the password as a piece of sensitive information
        self.mock_io.collect_sensitive.assert_called_with("Password: ")
        # The command should not persist the credentials
        self.mock_creds_persist.persist.assert_called_with(creds)
        # And the flow should return the collected credentials
        self.assertEqual(creds.username, "tcbakes")
        self.assertEqual(creds.password, "fake-o")
