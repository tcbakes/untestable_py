import sys
from untestable.di.console_config import APP

__version__ = '1.1.0rc0'


def main():
    """Entry point for the application script"""
    APP.run(sys.argv[1:])
